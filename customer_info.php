<?php 
$user='root';
$pass='';
$dbh=new PDO('mysql:host=localhost;dbname=hotel_management_system',$user,$pass);
if(!$dbh)
echo "connection with database failed!";


?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css">
<link rel="stylesheet" href="css/style.css">
<title>Hotel</title>
</head>
<body>




<h1 class="text-center m-4">Panevėžio Viešbutis</h1>






<!-- this is navbar -->

<nav class="navbar  navbar-expand-sm navbar-dark  text-black bg-secondary  justify-content-center" id="navbar">



<!-- Collapse button -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
        aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<!-- Collapsible content -->
<div class="collapse navbar-collapse text-center" id="basicExampleNav" toggle="collapse" data-target="navbar-collapse" style="outline-color: black;">

    <!-- Links -->
    <ul class="navbar-nav  text-center" style="margin-left: 400px;">
    <li class="nav-item">
<a class="nav-link act" href="index.php" >Pagrindinis</a>
</li>
        <li class="nav-item ">
            <a class="nav-link act " href="room_reservation.php">Kambario užsakymas</a>
                
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link act " href="customer_info.php">Klientų informacija</a>
        </li>
      
        <li class="nav-item ">
            <a class="nav-link act " href="classes.php">Klasės</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="room_numbers.php">Kambario numeriai</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="places.php">Vietos</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="queries.php">Užklausos</a>
        </li>

    </ul>
    
    <!-- Links -->                
</div>
<!-- Collapsible content -->
</nav>    




<div class="page-header header-filter" >
    <div class="container">
    <div class="title text-center display-4 m-4">
        Klientų informacija 
    </div>
        <div class="row">
            <br>
           
            <div class="col-md-4">
               <?php //for edit click 
                $name=isset($_POST["name"]) ? $_POST["name"] : "";
                
                $sname=isset($_POST["surname"]) ? $_POST["surname"] : "";
                $city=isset($_POST["city"]) ? $_POST["city"] : "0";
                $room_no=isset($_POST["room_no"]) ? $_POST["room_no"] : "0";
                $leave_date=isset($_POST["leave_date"]) ? $_POST["leave_date"] : "";
                $arrive_date=isset($_POST["arrive_date"]) ? $_POST["arrive_date"] : "";
                $xtra_price=isset($_POST["additional_price"]) ? $_POST["additional_price"] : "";
                
                $dateTime= new DateTime();
                $createdOn=$dateTime->format('Y-m-d H:i:s'); 
                
                if(isset($_REQUEST["id"])){
                    $id=$_REQUEST["id"];
                
                    $sql="SELECT name,surname,city,arrive_date,left_date,additional_price,room_no FROM customer_info where cust_id=:id";
                    $stmt=$dbh->prepare($sql);
                    $stmt->bindParam(':id',$id);
                    $stmt->execute();
                   $result= $stmt->fetchAll();
                    
                   if($stmt->errorCode() != 0){
                    die(print_r($stmt->errorInfo()));
                    }
                    else{    
        
                    
                    foreach($result as $row){
                        $name=$row["name"];
                        $sname=$row["surname"];
                        $city=$row["city"];
                        $arrive_date=$row["arrive_date"];
                        $leave_date=$row["left_date"];
                        $xtra_price=$row["additional_price"];
                        $room_no=$row["room_no"];
                      

                    }
                }
                }
               
               
               
               ?> 
           
           <form method="POST" action="">
    <div class="form-group">
 <label for="knumeris">Vardas:</label>
  <input type="text" class="form-control" id="name" name="name" value="<?php echo $name ?>">
    </div>
    <div class="form-group">
 <label for="knumeris">Pavardė:</label>
  <input type="text" class="form-control" id="surname" name="surname" value="<?php echo $sname ?>">
    </div>
    
    <div class="form-group">
         <label for="class">Iš kur atvyko:</label>
        <select class="form-control" id="class" name="city" >
            <option value="<?php echo $city ?>">
            Miestas
            </option>
            <?php   
                $sql = "SELECT city_id,city_name FROM city";
                $stmt=$dbh->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if($stmt->errorCode() != 0){
                  die(print_r($stmt->errorInfo()));
                  }
                  else{    
                foreach($result as $row){
                $id = $row["city_id"];
                $name = $row["city_name"];
                echo "<option value='$id'>$name</option>";
                                                        }
                                                        
                                                    }				
                                                    ?>
                                        </select>
                                        
                                        </div>


 <div class="form-group">
 <label for="knumeris">Kada atvyko:</label>
  <input type="date" class="form-control" id="arrive_date" name="arrive_date" value="<?php echo $arrive_date ?>">
    </div>
    <div class="form-group">
 <label for="knumeris">Kada išvyko:</label>
  <input type="date" class="form-control" id="leave-date" name="leave_date" value="<?php echo $leave_date ?>">
    </div>
    <div class="form-group">
 <label for="knumeris">Papildomų aptarnavimų kaina:</label>
  <input type="text" class="form-control" id="price" name="additional_price" value="<?php echo $xtra_price ?>">
    </div>

<div class="form-group">
         <label for="no of places">Kambario numeris : </label>
        <select class="form-control" id="no_of_places" name="room_no" >
            <option value="<?php echo $room_no ?>">
            --Pasirinkti--
            </option>
            <?php 
             
               for($i=1;$i<=30;$i++){
                   
                echo "<option value='$i'>$i</option>";
               }				
                                                ?>
                                        </select>
                                        
                                        </div>


                                        <div class="form-group">
    <div class="form-group">
 
  <input type="submit" class="form-control btn btn-submit bg-secondary text-white" id="btnsave" name="btnsave" value="Išsaugoti">
    </div>
    </form>

  <?php
$type = isset($_GET['type']) ? $_GET['type'] : '';
  $btnsave=isset($_POST["btnsave"]) ? true : false;
  $btnedit=isset($_POST["btnedit"]) ? true : false;
    
  if($btnsave && $type=='edit'){        //for edit data and update
    $name=isset($_POST["name"]) ? $_POST["name"] : "";
    $sname=isset($_POST["surname"]) ? $_POST["surname"] : "";
    $city=isset($_POST["city"]) ? $_POST["city"] : "0";
    $room_no=isset($_POST["room_no"]) ? $_POST["room_no"] : "0";
    $leave_date=isset($_POST["leave_date"]) ? $_POST["leave_date"] : "";
    $arrive_date=isset($_POST["arrive_date"]) ? $_POST["arrive_date"] : "";
    $xtra_price=isset($_POST["additional_price"]) ? $_POST["additional_price"] : "";
    $dateTime= new DateTime();
    $createdOn=$dateTime->format('Y-m-d H:i:s'); 
    $id=$_REQUEST["id"];
    $sql="UPDATE customer_info SET name=:name, surname=:surname, city=:city, arrive_date=:arrive_date,left_date=:left_date,additional_price=:xtra_price,room_no=:room_no ,dateCreated=:createdOn WHERE cust_id=:id";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam(':name',$name);
    $stmt->bindParam(':surname',$sname);
    $stmt->bindParam(':city',$city);
    $stmt->bindParam(':arrive_date',$arrive_date);
    $stmt->bindParam(':left_date',$leave_date);
    $stmt->bindParam(':xtra_price',$xtra_price);
    $stmt->bindParam(':room_no',$room_no);
    $stmt->bindParam(':createdOn',$createdOn);
    
    $stmt->bindParam(':id',$id);
    $result=$stmt->execute();
    if($result)
    echo "Išsaugota!";
    else
    echo "Klaida!";
    
     }



else if($btnsave){          //for insert data
  $name=isset($_POST["name"]) ? $_POST["name"] : "";
  $sname=isset($_POST["surname"]) ? $_POST["surname"] : "";
  $city=isset($_POST["city"]) ? $_POST["city"] : "0";
  $room_no=isset($_POST["room_no"]) ? $_POST["room_no"] : "0";
  $leave_date=isset($_POST["leave_date"]) ? $_POST["leave_date"] : "";
  $arrive_date=isset($_POST["arrive_date"]) ? $_POST["arrive_date"] : "";
  $xtra_price=isset($_POST["additional_price"]) ? $_POST["additional_price"] : "";
  $dateTime= new DateTime();
  $createdOn=$dateTime->format('Y-m-d H:i:s'); 
  
      $sql="INSERT INTO customer_info(name,surname,city,arrive_date,left_date,additional_price,room_no,dateCreated) VALUES(:name,:surname,:city,:arrive_date,:left_date,:xtra_price,:room_no,:createdOn)";
      $stmt=$dbh->prepare($sql);
      $stmt->bindParam(':name',$name);
    $stmt->bindParam(':surname',$sname);
    $stmt->bindParam(':city',$city);
    $stmt->bindParam(':arrive_date',$arrive_date);
    $stmt->bindParam(':left_date',$leave_date);
    $stmt->bindParam(':xtra_price',$xtra_price);
    $stmt->bindParam(':room_no',$room_no);
    $stmt->bindParam(':createdOn',$createdOn);
      $result=$stmt->execute();
      if($result){
          echo "Išsaugota!";
          
      }
      } 
 
else if($type == 'delete'){         //delete data

    $id=$_REQUEST["id"];
    $sql="DELETE FROM customer_info Where cust_id=:id";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam(':id',$id);
    $result=$stmt->execute();
    if($result)
    echo "Pašalinta!";
    else
    echo "Klaida!";
}

  ?>













</div>
            </div>
            <div class="col-md-12">
            <table class="table table-striped">
    <thead>
      <tr>
      <th>Vardas</th>
        <th>Pavardė</th>
        <th>Iš kur atvyko</th>
        <th>Kada atvyko</th>
        <th>Kada išvyko</th>
        <th>Papildomų aptarnavimų kaina</th>        
        <th>Kambario numeris</th>
        <th>Veiksmai</th>
      </tr>
    </thead>
    <?php //showing data from database
    $sql="SELECT * FROM customer_info,city where customer_info.city=city.city_id";
    $stmt=$dbh->prepare($sql);
    $stmt->execute();
    $result=$stmt->fetchAll();

    if($stmt->errorCode() != 0){
        die(print_r($stmt->errorInfo()));
        }
        else{    

        
        foreach($result as $row){
          $id=$row["cust_id"];
          $fname=$row["name"];
          $lname=$row["surname"];
          $cityName=$row["city_name"];
          $date_arrive=$row["arrive_date"];
          $date_left=$row["left_date"];
          $AdditionalPrice=$row["additional_price"];
          $roomNumber=$row["room_no"];
             ?>   
            
    <tbody>
      <tr>
      <td><?php echo $fname ?></td>
        <td><?php echo $lname ?></td>
        <td><?php echo $cityName ?></td>
        <td><?php echo $date_arrive ?></td>
        <td><?php echo $date_left ?></td>
        <td><?php echo $AdditionalPrice ?></td>
        <td><?php echo $roomNumber ?></td>
        <td><a type="submit" name="btnupdate" href="<?php echo  "customer_info.php?type=edit&id={$id}"?>">Redaguoti</a> / <a type="submit" name="btndel" id="del" class="delete" href="<?php echo "customer_info.php?type=delete&id={$id}" ?>" data-confirm="Ar tikrai norite pašalinti?" >Pašalinti</a></td>
                
            </tr>
            <?php
            }

        }
  
            ?>
    </tbody>
  </table>
            </div>
        
    </div>
</div>
<script>
var deleteLinks = document.querySelectorAll('.delete');

for (var i = 0; i < deleteLinks.length; i++) {
  deleteLinks[i].addEventListener('click', function(event) {
      event.preventDefault();

      var choice = confirm(this.getAttribute('data-confirm'));

      if (choice) {

        window.location.href = this.getAttribute('href');
        
    }
  });
}


</script>
</body>

</html>























