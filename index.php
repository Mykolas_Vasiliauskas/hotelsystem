
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css">
<link rel="stylesheet" href="css/style.css">
<title>Hotel</title>
</head>
<body>

<h1 class="text-center m-4">Panevėžio Viešbutis</h1>






<!-- this is navbar -->

<nav class="navbar  navbar-expand-sm navbar-dark  text-black bg-secondary  justify-content-center" id="navbar">



<!-- Collapse button -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
        aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<!-- Collapsible content -->
<div class="collapse navbar-collapse text-center" id="basicExampleNav" toggle="collapse" data-target="navbar-collapse" style="outline-color: black;">

    <!-- Links -->
    <ul class="navbar-nav  text-center" style="margin-left: 400px;">
    <li class="nav-item">
<a class="nav-link act active" href="index.php" active>Pagrindinis</a>
</li>
        <li class="nav-item ">
            <a class="nav-link act " href="room_reservation.php">Kambario užsakymas</a>
                
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link act " href="customer_info.php">Klientų informacija</a>
        </li>
      
        <li class="nav-item ">
            <a class="nav-link act " href="classes.php">Klasės</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="room_numbers.php">Kambario numeriai</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="places.php">Vietos</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="queries.php">Užklausos</a>
        </li>

    </ul>
    
    <!-- Links -->                
</div>
<!-- Collapsible content -->
</nav>    


<div class="page-header header-filter" >
    <div class="container">
        <div class="row">
            <br>
            <div class="col-md-12">
                    <h1 class="text-center mt-5 display-1">Sveiki atvykę!</h1> 

            </div>
        </div>
    </div>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>