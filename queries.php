<?php 
$user='root';
$pass='';
$dbh=new PDO('mysql:host=localhost;dbname=hotel_management_system',$user,$pass);
if(!$dbh)
echo "connection with database failed!";


?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css">
<link rel="stylesheet" href="css/style.css">
<title>Hotel</title>
</head>
<body>






<h1 class="text-center m-4">Panevėžio Viešbutis</h1>






<!-- this is navbar -->

<nav class="navbar  navbar-expand-sm navbar-dark  text-black bg-secondary  justify-content-center" id="navbar">



<!-- Collapse button -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
        aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<!-- Collapsible content -->
<div class="collapse navbar-collapse text-center" id="basicExampleNav" toggle="collapse" data-target="navbar-collapse" style="outline-color: black;">

    <!-- Links -->
    <ul class="navbar-nav  text-center" style="margin-left: 400px;">
    <li class="nav-item">
<a class="nav-link act" href="index.php" >Home</a>
</li>
        <li class="nav-item ">
            <a class="nav-link act " href="room_reservation.php">Room Reservations
                
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link act " href="customer_info.php">Customer Information</a>
        </li>
      
        <li class="nav-item ">
            <a class="nav-link act " href="classes.php" >Classes</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="room_numbers.php">Room Numbers</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="places.php">Places</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act active"  href="queries.php" active>Queries</a>
        </li>

    </ul>
    
    <!-- Links -->                
</div>
<!-- Collapsible content -->
</nav>  

<div>

<ul class="navbar-nav  text-center" style="margin-left: 100px;">
    <li class="nav-item">

<br>
<br>

        <a href = "Available rooms reservation.html">Laisvų kambarių rezervacija</a>
<br>
<br>
        <a href = "Customer stay place.html">Kliento apsistojimo vieta</a>
<br>
<br>
        <a href = "Available rooms.html">Laisvi kambariai</a>
<br>
<br>
        <a href = "List of existing customers.html">Esamų klientų sąrašas</a>
<br>
<br>
        <a href = "Invoice for customer.html">Saskaita klientui</a>
 
        </li>

</ul>
        </div>

        </body>
</html>