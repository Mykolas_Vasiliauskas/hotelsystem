<?php 
$user='root';
$pass='';
$dbh=new PDO('mysql:host=localhost;dbname=hotel_management_system',$user,$pass);
if(!$dbh)
echo "connection with database failed!";


?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css">
<link rel="stylesheet" href="css/style.css">
<title>Hotel</title>
</head>
<body>




<h1 class="text-center m-4">Panevėžio Viešbutis</h1>






<!-- this is navbar -->

<nav class="navbar  navbar-expand-sm navbar-dark  text-black bg-secondary  justify-content-center" id="navbar">



<!-- Collapse button -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
        aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<!-- Collapsible content -->
<div class="collapse navbar-collapse text-center" id="basicExampleNav" toggle="collapse" data-target="navbar-collapse" style="outline-color: black;">

    <!-- Links -->
    <ul class="navbar-nav  text-center" style="margin-left: 400px;">
    <li class="nav-item">
<a class="nav-link act" href="index.php" >Pagrindinis</a>
</li>
        <li class="nav-item ">
            <a class="nav-link act " href="room_reservation.php">Kambario užsakymas</a>
                
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link act " href="customer_info.php">Klientų informacija</a>
        </li>
      
        <li class="nav-item ">
            <a class="nav-link act active" href="classes.php" active>Klasės</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="room_numbers.php">Kambarių numeriai</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="places.php">Vietos</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="queries.php">Užklausos</a>
        </li>

    </ul>
    
    <!-- Links -->                
</div>
<!-- Collapsible content -->
</nav>    




<div class="page-header header-filter" >
    <div class="container">
    <div class="title text-center display-4 m-4">
        Klasės
    </div>
        <div class="row">
            <br>
           
            <div class="col-md-4">
               <?php //for edit click 
                
                $class=isset($_POST["class"]) ? $_POST["class"] : "";
                if(isset($_REQUEST["id"])){
                    $id=$_REQUEST["id"];
                    $sql="SELECT * FROM class  where room_id = :id";
                    $stmt=$dbh->prepare($sql);
                    $stmt->bindParam(':id',$id);
                    $stmt->execute();
                   $result= $stmt->fetchAll();
                    
                   if($stmt->errorCode() != 0){
                    die(print_r($stmt->errorInfo()));
                    }
                    else{    
        
                    
                    foreach($result as $row){
                      $class=$row["class_name"]; 
                        

                    }
                }
                }
               
               
               
               ?> 
           
           <form method="POST" action="">
    <div class="form-group">
 <label for="knumeris">Klasės:</label>
  <input type="text" class="form-control" id="name" name="class" value="<?php echo $class ?>">
    </div>
    
  <input type="submit" class="form-control btn btn-submit bg-secondary text-white" id="btnsave" name="btnsave" value="Save">
    </div>
    </form>

  <?php
$type = isset($_GET['type']) ? $_GET['type'] : '';
  $btnsave=isset($_POST["btnsave"]) ? true : false;
  $btnedit=isset($_POST["btnedit"]) ? true : false;
    
  if($btnsave && $type=='edit'){        //for edit data and update
    
    $class=isset($_POST["class"]) ? $_POST["class"] : "";
    $id=$_REQUEST["id"];
    
    $sql="UPDATE class SET class_name=:class_name WHERE class_id=:id";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam(':class_name',$class);
    $stmt->bindParam(':id',$id);
    $result=$stmt->execute();
    if($result)
    echo "Išsaugota!";
    else
    echo "Nepavyko!";
    
     }



else if($btnsave){          //for insert data
  $class=isset($_POST["class"]) ? $_POST["class"] : "";
  
      $sql="INSERT INTO class(class_name) VALUES(:class_name)";
      $stmt=$dbh->prepare($sql);
      $stmt->bindParam(':class_name',$class);
      $result=$stmt->execute();
      if($result){
          echo "Išsaugota!";
          
      }
      } 
 
else if($type == 'delete'){         //delete data

    $id=$_REQUEST["id"];
    $sql="DELETE FROM class Where class_id=:id";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam(':id',$id);
    $result=$stmt->execute();
    if($result)
    echo "Pašalinta!";
    else
    echo "Nepavyko Pašalinti!";
}

  ?>












        

            </div>
            <table class="table table-striped">
    <thead>
      <tr>
      <th>Klasės</th>
        
        <th>Veiksmai</th>
      </tr>
    </thead>
    <?php //showing data from database
    $sql="SELECT * FROM class";
    $stmt=$dbh->prepare($sql);
    $stmt->execute();
    $result=$stmt->fetchAll();

    if($stmt->errorCode() != 0){
        die(print_r($stmt->errorInfo()));
        }
        else{    

        
        foreach($result as $row){
          $id=$row["class_id"];
          $class=$row["class_name"];
             ?>   
            
    <tbody>
      <tr>
      <td><?php echo $class ?></td>
        <td><a type="submit" name="btnupdate"  href="<?php echo  "classes.php?type=edit&id={$id}"?>">Redaguoti</a> / <a type="submit" name="btndel" id="del" class="delete" href="<?php echo "classes.php?type=delete&id={$id}" ?>" data-confirm="Ar tikrai norite pašalinti?" >Šalinti</a></td>
                
            </tr>
            <?php
            }

        }
  
            ?>
    </tbody>
  </table>

        </div>
    </div>
</div>
<script>
var deleteLinks = document.querySelectorAll('.delete');

for (var i = 0; i < deleteLinks.length; i++) {
  deleteLinks[i].addEventListener('click', function(event) {
      event.preventDefault();

      var choice = confirm(this.getAttribute('data-confirm'));

      if (choice) {

        window.location.href = this.getAttribute('href');
        
    }
  });
}


</script>
</body>

</html>
