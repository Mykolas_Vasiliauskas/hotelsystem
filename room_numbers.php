<?php 
$user='root';
$pass='';
$dbh=new PDO('mysql:host=localhost;dbname=hotel_management_system',$user,$pass);
if(!$dbh)
echo "connection with database failed!";


?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css">
<link rel="stylesheet" href="css/style.css">
<title>Hotel</title>
</head>
<body>




<h1 class="text-center m-4">Panevėžio Viešbutis</h1>






<!-- this is navbar -->

<nav class="navbar  navbar-expand-sm navbar-dark  text-black bg-secondary  justify-content-center" id="navbar">



<!-- Collapse button -->
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
        aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<!-- Collapsible content -->
<div class="collapse navbar-collapse text-center" id="basicExampleNav" toggle="collapse" data-target="navbar-collapse" style="outline-color: black;">

    <!-- Links -->
    <ul class="navbar-nav  text-center" style="margin-left: 400px;">
    <li class="nav-item">
<a class="nav-link act" href="index.php" >Pagrindinis</a>
</li>
        <li class="nav-item ">
            <a class="nav-link act " href="room_reservation.php">Kambario užsakymas</a>
                
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link act " href="customer_info.php">Klientų informacija</a>
        </li>
      
        <li class="nav-item ">
            <a class="nav-link act " href="classes.php">Klasės</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="room_numbers.php">Kambario numeriai</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="places.php">Vietos</a>
        </li>
        <li class="nav-item vertical-line">
            <a class="nav-link act "  href="queries.php">Užklausos</a>
        </li>

    </ul>
    
    <!-- Links -->                
</div>
<!-- Collapsible content -->
</nav>    




<div class="page-header header-filter" >
    <div class="container">
    <div class="title text-center display-4 m-4">
         Kambario numeriai
    </div>
        <div class="row">
            <br>
           
            <div class="col-md-4">
               <?php //for edit click 
                
                $room_no=isset($_POST["room_no"]) ? $_POST["room_no"] : "";
                if(isset($_REQUEST["id"])){
                    $id=$_REQUEST["id"];
                    $sql="SELECT * FROM room_numbers  where room_id = :id";
                    $stmt=$dbh->prepare($sql);
                    $stmt->bindParam(':id',$id);
                    $stmt->execute();
                   $result= $stmt->fetchAll();
                    
                   if($stmt->errorCode() != 0){
                    die(print_r($stmt->errorInfo()));
                    }
                    else{    
        
                    
                    foreach($result as $row){
                      $room_no=$row["room_no"]; 
                        

                    }
                }
                }
               
               
               
               ?> 
           
           <form method="POST">
    <div class="form-group">
         <label for="no of places">Kambario numeris : </label>
        <select class="form-control" id="no_of_places" name="room_no" >
            <option value="<?php echo $room_no ?>">
            --Pasirinkti--
            </option>
            <?php 
             
               for($i=1;$i<=30;$i++){
                   
                echo "<option value='$i'>$i</option>";
               }				
                                                ?>
                                        </select>
                                        
                                        </div>

    
  <input type="submit" class="form-control btn btn-submit bg-secondary text-white" id="btnsave" name="btnsave" value="Išsaugoti">
    </div>
    </form>

  <?php
$type = isset($_GET['type']) ? $_GET['type'] : '';
  $btnsave=isset($_POST["btnsave"]) ? true : false;
  $btnedit=isset($_POST["btnedit"]) ? true : false;
    
  if($btnsave && $type=='edit'){        //for edit data and update
    
    $room_no=isset($_POST["room_no"]) ? $_POST["room_no"] : "";
    $id=$_REQUEST["id"];
    
    $sql="UPDATE room_numbers SET room_no=:room_no WHERE room_id=:id";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam(':room_no',$room_no);
    $stmt->bindParam(':id',$id);
    $result=$stmt->execute();
    if($result)
    echo "Išsaugota!";
    else
    echo "Klaida!";
    
     }



else if($btnsave){          //for insert data
  $room_no=isset($_POST["room_no"]) ? $_POST["room_no"] : "";
  
      $sql="INSERT INTO room_numbers(room_no) VALUES(:room_no)";
      $stmt=$dbh->prepare($sql);
      $stmt->bindParam(':room_no',$room_no);
      $result=$stmt->execute();
      if($result){
          echo "Išsaugota!";
          
      }
      } 
 
else if($type == 'delete'){         //delete data

    $id=$_REQUEST["id"];
    $sql="DELETE FROM room_numbers Where room_id=:id";
    $stmt=$dbh->prepare($sql);
    $stmt->bindParam(':id',$id);
    $result=$stmt->execute();
    if($result)
    echo "Pašalinta!";
    else
    echo "Klaida!";
}

  ?>












        

            </div>
            <table class="table table-striped">
    <thead>
      <tr>
      <th>Kambario numeris</th>
        
        <th>Veiksmai</th>
      </tr>
    </thead>
    <?php //showing data from database
    $sql="SELECT * FROM room_numbers";
    $stmt=$dbh->prepare($sql);
    $stmt->execute();
    $result=$stmt->fetchAll();

    if($stmt->errorCode() != 0){
        die(print_r($stmt->errorInfo()));
        }
        else{    

        
        foreach($result as $row){
          $id=$row["room_id"];
                    $room_no=$row["room_no"];
             ?>   
            
    <tbody>
      <tr>
      <td><?php echo $room_no ?></td>
        <td><a type="submit" name="btnupdate"  href="<?php echo  "room_numbers.php?type=edit&id={$id}"?>">Redaguoti</a> / <a type="submit" name="btndel" id="del" class="delete" href="<?php echo "room_numbers.php?type=delete&id={$id}" ?>" data-confirm="Ar tikrai norite pašalinti?" >Pašalinti</a></td>
                
            </tr>
            <?php
            }

        }
  
            ?>
    </tbody>
  </table>

        </div>
    </div>
</div>
<script>
var deleteLinks = document.querySelectorAll('.delete');

for (var i = 0; i < deleteLinks.length; i++) {
  deleteLinks[i].addEventListener('click', function(event) {
      event.preventDefault();

      var choice = confirm(this.getAttribute('data-confirm'));

      if (choice) {

        window.location.href = this.getAttribute('href');
        
    }
  });
}


</script>
</body>

</html>
